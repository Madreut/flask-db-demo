FROM rackspacedot/python37

VOLUME /myapp
WORKDIR /myapp
RUN pip install -r req.txt 
RUN  mkdir /myapp/app
COPY web_app.py web_app.db /myapp
COPY app/* /myapp/app
EXPOSE 8080
ENTRYPOINT ["python3", "web_app.py"]

